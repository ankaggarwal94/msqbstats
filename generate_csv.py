#! venv/bin/python
import mysql.connector as mariadb

connection = mariadb.connect(user='msqbstats', database='msqbstats')
cursor = connection.cursor()

for year in range(2010, 2018):

    f = open('data/{}.csv'.format(year), 'w')

    cursor.execute("select team,name as tourney_name,qset,tourney_date,ppb,powers,gets,negs,oppb,opowers,ogets,onegs,path"
               " from results join tourneys on results.tourney = tourneys.id"
               " where (year(tourney_date) = {} and month(tourney_date) > 7)"
               " or (year(tourney_date) = {} and month(tourney_date) < 8)"
               .format(str(year), str(year - 1)))

    for c in cursor:
        s = "'{}','{}','{}',{},{},{},{},{},{},{},{},{},{}\n".format(
            c[0], c[1], c[2], str(c[3]), str(c[4]), c[5], c[6], c[7], str(c[8]), c[9], c[10], c[11], c[12]
        )
        f.write(s)

    f.close()
